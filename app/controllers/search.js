import Ember from 'ember';
import $ from 'jquery';

export default Ember.Controller.extend({
  flickr: Ember.inject.service(),
  photos: null,

  actions: {
    search(searchTerm) {
      this.get('flickr').search(searchTerm).then(res => {
        this.set('photos', res);
      });
    }
  }
});
